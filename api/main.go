package main

import (
	"log"
	"net/http"

	"bitbucket.org/ValentinLethiot/userEvent/domain/projection"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/publisher/mypublisher"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/store/redisstore"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/storeClient"
)

func main() {
	redstore := redisstore.NewRedisStore("localhost:6379", "", 0)

	projection := projection.NewUserDetailsProjection()
	cli := storeClient.NewStoreSub(*redstore, "mystream", "myAPIgroup", projection)

	storePub := storeClient.NewStorePub(*redstore, "mystream")
	publisher := mypublisher.NewMyPublisher(*storePub)

	r := infrastructure.GetRouter(publisher, projection)

	go cli.ReadMyStore(nil)

	log.Println("Server started on port 8080")
	http.ListenAndServe(":8080", r)
}
