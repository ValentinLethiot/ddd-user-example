package command

type UpdateAgeCommand struct {
	ID     int
	NewAge int
}

func NewUpdateAgeCommand(id, new int) *UpdateAgeCommand {
	return &UpdateAgeCommand{
		ID:     id,
		NewAge: new,
	}
}
