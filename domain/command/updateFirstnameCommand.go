package command

type UpdateFirstnameCommand struct {
	ID           int
	NewFirstname string
}

func NewUpdateFirstnameCommand(id int, firstname string) *UpdateFirstnameCommand {
	return &UpdateFirstnameCommand{
		ID:           id,
		NewFirstname: firstname,
	}
}
