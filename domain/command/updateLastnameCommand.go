package command

type UpdateLastnameCommand struct {
	ID          int
	NewLastname string
}

func NewUpdateLastnameCommand(id int, lastname string) *UpdateLastnameCommand {
	return &UpdateLastnameCommand{
		ID:          id,
		NewLastname: lastname,
	}
}
