package command

type UpdatePasswordCommand struct {
	ID          int
	NewPassword string
}

func NewUpdatePasswordCommand(id int, newPass string) UpdatePasswordCommand {
	return UpdatePasswordCommand{
		ID:          id,
		NewPassword: newPass,
	}
}
