package event

type AgeUpdatedEvent struct {
	ID  int
	Age int
}

func NewAgeUpdatedEvent(id, age int) *AgeUpdatedEvent {
	return &AgeUpdatedEvent{
		ID:  id,
		Age: age,
	}
}

func (aue AgeUpdatedEvent) GetID() int {
	return aue.ID
}