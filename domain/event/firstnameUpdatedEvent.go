package event

type FirstnameUpdatedEvent struct {
	ID int
	NewFirstname string
}

func NewFirstnameUpdatedEvent(id int, firstname string) *FirstnameUpdatedEvent {
	return &FirstnameUpdatedEvent{
		ID : id,
		NewFirstname: firstname,
	}
}

func (fue FirstnameUpdatedEvent) GetID() int {
	return fue.ID
}
