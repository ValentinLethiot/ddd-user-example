package event

type LastnameUpdatedEvent struct {
	ID          int
	NewLastname string
}

func NewLastnameUpdatedEvent(id int, lastname string) *LastnameUpdatedEvent {
	return &LastnameUpdatedEvent{
		ID:          id,
		NewLastname: lastname,
	}
}

func (lue LastnameUpdatedEvent) GetID() int {
	return lue.ID
}
