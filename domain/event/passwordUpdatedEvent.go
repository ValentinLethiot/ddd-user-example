package event

type PasswordUpdatedEvent struct {
	UserEvent
	ID          int
	NewPassword string
}

func NewPasswordUpdatedEvent(id int, pass string) *PasswordUpdatedEvent {
	return &PasswordUpdatedEvent{
		ID:          id,
		NewPassword: pass,
	}
}

func (pue PasswordUpdatedEvent) GetID() int {
	return pue.ID
}
