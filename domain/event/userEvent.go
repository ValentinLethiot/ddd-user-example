package event

type UserEvent interface {
	GetID() int
}
