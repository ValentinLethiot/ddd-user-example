package event

type UserRegisteredEvent struct {
	UserEvent
	ID        int
	Firstname string
	Lastname  string
	Age       int
	Email     string
	Password  string
}

func NewUserRegisteredEvent(id, age int, fn, ln, mail, pass string) *UserRegisteredEvent {
	return &UserRegisteredEvent{
		ID:        id,
		Firstname: fn,
		Lastname:  ln,
		Age:       age,
		Email:     mail,
		Password:  pass,
	}
}

func (ure UserRegisteredEvent) GetID() int {
	return ure.ID
}
