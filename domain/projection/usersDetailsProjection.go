package projection

import (
	"fmt"
	"log"

	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
)

type User struct {
	ID        int
	Firstname string
	Lastname  string
	Age       int
	Email     string
	Password  string
}

func NewUser(id, age int, fn, ln, mail, pass string) *User {
	return &User{
		ID:        id,
		Firstname: fn,
		Lastname:  ln,
		Age:       age,
		Email:     mail,
		Password:  pass,
	}
}

type UsersDetailsProjection struct {
	Users  map[int]*User
	Events []event.UserEvent
}

func NewUserDetailsProjection() *UsersDetailsProjection {
	return &UsersDetailsProjection{
		Users:  make(map[int]*User, 0),
		Events: make([]event.UserEvent, 0),
	}
}

func NewUserDetailsProjectionFromEvents(events []event.UserEvent) *UsersDetailsProjection {

	pr := NewUserDetailsProjection()

	for _, ev := range events {
		switch ev.(type) {
		case *event.UserRegisteredEvent:
			ure := ev.(*event.UserRegisteredEvent)
			pr.applyUserRegisteredEvent(*ure)
			pr.Events = append(pr.Events, ure)

		case *event.PasswordUpdatedEvent:
			ure := ev.(*event.PasswordUpdatedEvent)
			pr.applyUpdatePasswordEvent(*ure)
			pr.Events = append(pr.Events, ure)

		case *event.FirstnameUpdatedEvent:
			ure := ev.(*event.FirstnameUpdatedEvent)
			pr.applyUpdateFirstnameEvent(*ure)
			pr.Events = append(pr.Events, ure)

		case *event.LastnameUpdatedEvent:
			ure := ev.(*event.LastnameUpdatedEvent)
			pr.applyUpdateLastnameEvent(*ure)
			pr.Events = append(pr.Events, ure)

		case *event.AgeUpdatedEvent:
			ure := ev.(*event.AgeUpdatedEvent)
			pr.applyUpdateAgeEvent(*ure)
			pr.Events = append(pr.Events, ure)

		default:
			log.Fatalf("Unknow Event")
		}
	}
	return pr
}

func (p *UsersDetailsProjection) applyUserRegisteredEvent(ev event.UserRegisteredEvent) error {
	if _, exist := p.Users[ev.ID]; exist != false {
		return fmt.Errorf("User already exists")
	}

	us := NewUser(ev.ID, ev.Age, ev.Firstname, ev.Lastname, ev.Email, ev.Password)
	p.Users[ev.ID] = us

	return nil
}

func (p *UsersDetailsProjection) applyUpdatePasswordEvent(ev event.PasswordUpdatedEvent) error {
	if _, exist := p.Users[ev.ID]; exist == false {
		return fmt.Errorf("User doesn't exist")
	}

	p.Users[ev.ID].Password = ev.NewPassword
	return nil
}

func (p *UsersDetailsProjection) applyUpdateFirstnameEvent(ev event.FirstnameUpdatedEvent) error {
	if _, exist := p.Users[ev.ID]; exist == false {
		return fmt.Errorf("User doesn't exist")
	}

	p.Users[ev.ID].Firstname = ev.NewFirstname
	return nil
}

func (p *UsersDetailsProjection) applyUpdateLastnameEvent(ev event.LastnameUpdatedEvent) error {
	if _, exist := p.Users[ev.ID]; exist == false {
		return fmt.Errorf("User doesn't exist")
	}

	p.Users[ev.ID].Lastname = ev.NewLastname
	return nil
}

func (p *UsersDetailsProjection) applyUpdateAgeEvent(ev event.AgeUpdatedEvent) error {
	if _, exist := p.Users[ev.ID]; exist == false {
		return fmt.Errorf("User doesn't exist")
	}

	p.Users[ev.ID].Age = ev.Age
	return nil
}

func (p *UsersDetailsProjection) ApplyEvent(ev event.UserEvent) {
	switch ev.(type) {
	case *event.UserRegisteredEvent:
		ure := ev.(*event.UserRegisteredEvent)
		p.applyUserRegisteredEvent(*ure)
		p.Events = append(p.Events, ure)

	case *event.PasswordUpdatedEvent:
		ure := ev.(*event.PasswordUpdatedEvent)
		p.applyUpdatePasswordEvent(*ure)
		p.Events = append(p.Events, ure)

	case *event.FirstnameUpdatedEvent:
		ure := ev.(*event.FirstnameUpdatedEvent)
		p.applyUpdateFirstnameEvent(*ure)
		p.Events = append(p.Events, ure)

	case *event.LastnameUpdatedEvent:
		ure := ev.(*event.LastnameUpdatedEvent)
		p.applyUpdateLastnameEvent(*ure)
		p.Events = append(p.Events, ure)

	case *event.AgeUpdatedEvent:
		ure := ev.(*event.AgeUpdatedEvent)
		p.applyUpdateAgeEvent(*ure)
		p.Events = append(p.Events, ure)

	default:
		log.Fatalf("Unknow Event")
	}

}

func (p *UsersDetailsProjection) ApplyEvents(evs []event.UserEvent) {
	for i := 0; i < len(evs); i++ {
		p.ApplyEvent(evs[i])
	}
}

func (p *UsersDetailsProjection) GetEvents() []event.UserEvent {
	return p.Events
}

func (p *UsersDetailsProjection) GetEventsByUserID(id int) []event.UserEvent {
	evtsById := make([]event.UserEvent, 0)
	evts := p.GetEvents()
	for _, evt := range evts {
		if evt.GetID() == id {
			evtsById = append(evtsById, evt)
		}
	}
	return evtsById
}
