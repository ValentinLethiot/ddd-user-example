package projection

import (
	"reflect"
	"testing"

	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
	"github.com/matryer/is"
)

func TestUserDetailsProjectionOneUser_returnsOK(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
	}

	//When
	p := NewUserDetailsProjectionFromEvents(events)

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(1, len(p.Users))
	is.Equal(200, p.Users[1].Age)
	is.Equal("jon", p.Users[1].Firstname)
	is.Equal("doe", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("password123", p.Users[1].Password)
}

func TestUserDetailsProjectionOneUserChangeData_returnsOK(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
		event.NewPasswordUpdatedEvent(1, "azerty123"),
		event.NewFirstnameUpdatedEvent(1, "jojojo"),
		event.NewLastnameUpdatedEvent(1, "dododo"),
		event.NewAgeUpdatedEvent(1, 20),
	}

	//When
	p := NewUserDetailsProjectionFromEvents(events)

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(1, len(p.Users))
	is.Equal(20, p.Users[1].Age)
	is.Equal("jojojo", p.Users[1].Firstname)
	is.Equal("dododo", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("azerty123", p.Users[1].Password)
}

func TestUserDetailsProjectionTwoUsersChangeData_returnsOK(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
		event.NewUserRegisteredEvent(2, 10, "alpha", "beta", "alphabet@gmail.com", "abcdefgh"),
		event.NewPasswordUpdatedEvent(1, "azerty123"),
		event.NewPasswordUpdatedEvent(2, "aaaaaaaa"),
		event.NewFirstnameUpdatedEvent(1, "jojojo"),
		event.NewFirstnameUpdatedEvent(2, "ahpla"),
		event.NewLastnameUpdatedEvent(1, "dododo"),
		event.NewLastnameUpdatedEvent(2, "ateb"),
		event.NewAgeUpdatedEvent(1, 20),
		event.NewAgeUpdatedEvent(2, 99),
	}

	//When
	p := NewUserDetailsProjectionFromEvents(events)

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(2, len(p.Users))
	is.Equal(20, p.Users[1].Age)
	is.Equal("jojojo", p.Users[1].Firstname)
	is.Equal("dododo", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("azerty123", p.Users[1].Password)

	is.Equal(99, p.Users[2].Age)
	is.Equal("ahpla", p.Users[2].Firstname)
	is.Equal("ateb", p.Users[2].Lastname)
	is.Equal("alphabet@gmail.com", p.Users[2].Email)
	is.Equal("aaaaaaaa", p.Users[2].Password)
}

func TestUserDetailsProjectionTwoUsers_returnsOK(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
		event.NewUserRegisteredEvent(2, 10, "alpha", "beta", "alphabet@gmail.com", "abcdefgh"),
	}

	//When
	p := NewUserDetailsProjectionFromEvents(events)

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(2, len(p.Users))
	is.Equal(200, p.Users[1].Age)
	is.Equal("jon", p.Users[1].Firstname)
	is.Equal("doe", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("password123", p.Users[1].Password)

	is.Equal(10, p.Users[2].Age)
	is.Equal("alpha", p.Users[2].Firstname)
	is.Equal("beta", p.Users[2].Lastname)
	is.Equal("alphabet@gmail.com", p.Users[2].Email)
	is.Equal("abcdefgh", p.Users[2].Password)
}

func TestUserDetailsProjectionTwoUsers_returnsAlreadyExists(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
		event.NewUserRegisteredEvent(1, 10, "alpha", "beta", "alphabet@gmail.com", "abcdefgh"),
	}

	//When
	p := NewUserDetailsProjectionFromEvents(events)

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(1, len(p.Users))
	is.Equal(200, p.Users[1].Age)
	is.Equal("jon", p.Users[1].Firstname)
	is.Equal("doe", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("password123", p.Users[1].Password)
}

func TestUserDetailsProjectionOneUserApplyUpdateAgeEvent_returnsOK(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
	}
	p := NewUserDetailsProjectionFromEvents(events)

	//When
	p.applyUpdateAgeEvent(*event.NewAgeUpdatedEvent(1, 20))

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(1, len(p.Users))
	is.Equal(20, p.Users[1].Age)
	is.Equal("jon", p.Users[1].Firstname)
	is.Equal("doe", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("password123", p.Users[1].Password)
}

func TestUserDetailsProjectionOneUserApplyUpdateAgeEvent_returnsNotExists(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
	}
	p := NewUserDetailsProjectionFromEvents(events)

	//When
	p.applyUpdateAgeEvent(*event.NewAgeUpdatedEvent(2, 20))

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(1, len(p.Users))
	is.Equal(200, p.Users[1].Age)
	is.Equal("jon", p.Users[1].Firstname)
	is.Equal("doe", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("password123", p.Users[1].Password)
}

func TestUserDetailsProjectionOneUserApplyUpdateFirstnameEvent_returnsOK(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
	}
	p := NewUserDetailsProjectionFromEvents(events)

	//When
	p.applyUpdateFirstnameEvent(*event.NewFirstnameUpdatedEvent(1, "jojojo"))

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(1, len(p.Users))
	is.Equal(200, p.Users[1].Age)
	is.Equal("jojojo", p.Users[1].Firstname)
	is.Equal("doe", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("password123", p.Users[1].Password)
}

func TestUserDetailsProjectionOneUserApplyUpdateFirstnameEvent_returnsNotExist(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
	}
	p := NewUserDetailsProjectionFromEvents(events)

	//When
	p.applyUpdateFirstnameEvent(*event.NewFirstnameUpdatedEvent(2, "jojojo"))

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(1, len(p.Users))
	is.Equal(200, p.Users[1].Age)
	is.Equal("jon", p.Users[1].Firstname)
	is.Equal("doe", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("password123", p.Users[1].Password)
}

func TestUserDetailsProjectionOneUserApplyUpdateLastnamenameEvent_returnsOK(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
	}
	p := NewUserDetailsProjectionFromEvents(events)

	//When
	p.applyUpdateLastnameEvent(*event.NewLastnameUpdatedEvent(1, "dododo"))

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(1, len(p.Users))
	is.Equal(200, p.Users[1].Age)
	is.Equal("jon", p.Users[1].Firstname)
	is.Equal("dododo", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("password123", p.Users[1].Password)
}

func TestUserDetailsProjectionOneUserApplyUpdateLastnamenameEvent_returnsNotExists(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
	}
	p := NewUserDetailsProjectionFromEvents(events)

	//When
	p.applyUpdateLastnameEvent(*event.NewLastnameUpdatedEvent(2, "dododo"))

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(1, len(p.Users))
	is.Equal(200, p.Users[1].Age)
	is.Equal("jon", p.Users[1].Firstname)
	is.Equal("doe", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("password123", p.Users[1].Password)
}
func TestUserDetailsProjectionOneUserApplyUpdatePasswordEvent_returnsOK(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
	}
	p := NewUserDetailsProjectionFromEvents(events)

	//When
	p.applyUpdatePasswordEvent(*event.NewPasswordUpdatedEvent(1, "azerty123"))

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(1, len(p.Users))
	is.Equal(200, p.Users[1].Age)
	is.Equal("jon", p.Users[1].Firstname)
	is.Equal("doe", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("azerty123", p.Users[1].Password)
}

func TestUserDetailsProjectionOneUserApplyUpdatePasswordEvent_returnsNotExist(t *testing.T) {
	//Given
	is := is.New(t)
	events := []event.UserEvent{
		event.NewUserRegisteredEvent(1, 200, "jon", "doe", "jjdd@gmail.com", "password123"),
	}
	p := NewUserDetailsProjectionFromEvents(events)

	//When
	p.applyUpdatePasswordEvent(*event.NewPasswordUpdatedEvent(2, "azerty123"))

	//Then
	is.Equal(reflect.TypeOf(p), reflect.TypeOf((*UsersDetailsProjection)(nil)))
	is.Equal(1, len(p.Users))
	is.Equal(200, p.Users[1].Age)
	is.Equal("jon", p.Users[1].Firstname)
	is.Equal("doe", p.Users[1].Lastname)
	is.Equal("jjdd@gmail.com", p.Users[1].Email)
	is.Equal("password123", p.Users[1].Password)
}
