package domain

import (
	"fmt"
	"log"

	"bitbucket.org/ValentinLethiot/userEvent/domain/command"
	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
)

type decisionProjection struct {
	ID        int
	Firstname string
	Lastname  string
	Age       int
	Email     string
	Password  string
}

func (dp *decisionProjection) applyUserRegisteredEvent(e event.UserRegisteredEvent) {
	dp.ID = e.ID
	dp.Firstname = e.Firstname
	dp.Lastname = e.Lastname
	dp.Age = e.Age
	dp.Email = e.Email
	dp.Password = e.Password
}

func (dp *decisionProjection) applyUpdatePasswordEvent(e event.PasswordUpdatedEvent) {
	dp.Password = e.NewPassword
}

func (dp *decisionProjection) applyUpdateAgeEvent(e event.AgeUpdatedEvent) {
	dp.Age = e.Age
}

func (dp *decisionProjection) applyUpdateFirstnameEvent(e event.FirstnameUpdatedEvent) {
	dp.Firstname = e.NewFirstname
}

func (dp *decisionProjection) applyUpdateLastnameEvent(e event.LastnameUpdatedEvent) {
	dp.Lastname = e.NewLastname
}

type UserAggregate struct {
	*decisionProjection
}

func NewUser() *UserAggregate {
	return &UserAggregate{
		decisionProjection: &decisionProjection{},
	}
}

func NewUserFromEvents(events []event.UserEvent) (*UserAggregate, error) {
	if len(events) == 0 {
		return nil, fmt.Errorf("No events")
	}

	us := NewUser()

	for _, ev := range events {
		switch ev.(type) {
		case *event.UserRegisteredEvent:
			ure := ev.(*event.UserRegisteredEvent)
			us.applyUserRegisteredEvent(*ure)

		case *event.PasswordUpdatedEvent:
			ure := ev.(*event.PasswordUpdatedEvent)
			us.applyUpdatePasswordEvent(*ure)

		case *event.FirstnameUpdatedEvent:
			ure := ev.(*event.FirstnameUpdatedEvent)
			us.applyUpdateFirstnameEvent(*ure)

		case *event.LastnameUpdatedEvent:
			ure := ev.(*event.LastnameUpdatedEvent)
			us.applyUpdateLastnameEvent(*ure)

		case *event.AgeUpdatedEvent:
			ure := ev.(*event.AgeUpdatedEvent)
			us.applyUpdateAgeEvent(*ure)

		default:
			log.Fatalf("Unknow Event")
		}
	}

	return us, nil
}

func (aggregate UserAggregate) String() string {
	return fmt.Sprint("{\nID : ", aggregate.ID, "\nFirstname : ", aggregate.Firstname, "\nLastname : ", aggregate.Lastname,
		"\nAge : ", aggregate.Age, "\nEmail : ", aggregate.Email, "\nPassword : ", aggregate.Password, "\n}")
}

func (aggregate UserAggregate) Register(registerUserCommand command.RegisterUserCommand) (*event.UserRegisteredEvent, error) {
	if len(registerUserCommand.Password) < 8 {
		return nil, fmt.Errorf("Too short password")
	}

	e := event.NewUserRegisteredEvent(
		registerUserCommand.ID,
		registerUserCommand.Age,
		registerUserCommand.Firstname,
		registerUserCommand.Lastname,
		registerUserCommand.Email,
		registerUserCommand.Password)

	aggregate.applyUserRegisteredEvent(*e)

	return e, nil
}

func (aggregate UserAggregate) ChangePassword(command command.UpdatePasswordCommand) (*event.PasswordUpdatedEvent, error) {
	if aggregate.Password == command.NewPassword {
		return nil, fmt.Errorf("Invalid password")
	}
	if len(command.NewPassword) < 8 {
		return nil, fmt.Errorf("Too short password")
	}

	e := event.NewPasswordUpdatedEvent(
		command.ID,
		command.NewPassword)

	aggregate.applyUpdatePasswordEvent(*e)

	return e, nil
}

func (aggregate UserAggregate) ChangeAge(cmd command.UpdateAgeCommand) (*event.AgeUpdatedEvent, error) {
	if cmd.NewAge < aggregate.Age {
		return nil, fmt.Errorf("New age is lower than the current")
	}

	e := event.NewAgeUpdatedEvent(cmd.ID, cmd.NewAge)

	aggregate.applyUpdateAgeEvent(*e)

	return e, nil
}

func (aggregate UserAggregate) ChangeFirstname(cmd command.UpdateFirstnameCommand) (*event.FirstnameUpdatedEvent, error) {
	if aggregate.Firstname == cmd.NewFirstname {
		return nil, fmt.Errorf("Firstname value incorrect")
	}

	e := event.NewFirstnameUpdatedEvent(cmd.ID, cmd.NewFirstname)

	aggregate.applyUpdateFirstnameEvent(*e)

	return e, nil
}

func (aggregate UserAggregate) ChangeLastname(cmd command.UpdateLastnameCommand) (*event.LastnameUpdatedEvent, error) {
	if aggregate.Lastname == cmd.NewLastname {
		return nil, fmt.Errorf("Lastname value incorrect")
	}

	e := event.NewLastnameUpdatedEvent(cmd.ID, cmd.NewLastname)

	aggregate.applyUpdateLastnameEvent(*e)

	return e, nil
}
