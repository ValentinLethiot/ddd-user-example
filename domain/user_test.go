package domain

import (
	"fmt"
	"reflect"
	"testing"

	"bitbucket.org/ValentinLethiot/userEvent/domain/command"
	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
	"github.com/matryer/is"
)

func TestRegisterUser_returnsUserRegistred(t *testing.T) {
	//Given
	is := is.New(t)
	command := command.NewRegisterUserCommand(1, 42, "jon", "doe", "jon.doe@gmail.com", "yack1234")
	user := NewUser()

	//When
	evt, err := user.Register(command)

	//Then
	is.NoErr(err)
	is.Equal(reflect.TypeOf(evt), reflect.TypeOf((*event.UserRegisteredEvent)(nil)))
	is.Equal(1, evt.ID)
	is.Equal(42, evt.Age)
	is.Equal("jon", evt.Firstname)
	is.Equal("doe", evt.Lastname)
	is.Equal("jon.doe@gmail.com", evt.Email)
	is.Equal("yack1234", evt.Password)
}

func TestRegisterUser_returnErrorWhenPasswordTooShort(t *testing.T) {
	//Given
	is := is.New(t)
	command := command.NewRegisterUserCommand(1, 42, "jon", "doe", "jon.doe@gmail.com", "1234")
	user := NewUser()

	//When
	evt, err := user.Register(command)

	//Then
	is.Equal(fmt.Errorf("Too short password"), err)
	is.Equal(nil, evt)
}

func TestUpdatePassword_returnPasswordUpdated(t *testing.T) {
	//Given
	is := is.New(t)
	ure := event.NewUserRegisteredEvent(1, 42, "jon", "doe", "jon.doe@gmail.com", "yack1234")
	user, _ := NewUserFromEvents([]event.UserEvent{ure})

	command := command.NewUpdatePasswordCommand(1, "yack+1234")

	//When
	evt, err := user.ChangePassword(command)

	//Then
	is.NoErr(err)
	is.Equal(reflect.TypeOf(evt), reflect.TypeOf((*event.PasswordUpdatedEvent)(nil)))
	is.Equal(1, evt.ID)
	is.Equal("yack+1234", evt.NewPassword)
}

func TestUpdatePassword_returnErrorWhenSamePassword(t *testing.T) {
	//Given
	is := is.New(t)
	ure := event.NewUserRegisteredEvent(1, 42, "jon", "doe", "jon.doe@gmail.com", "yack1234")
	user, _ := NewUserFromEvents([]event.UserEvent{ure})

	command := command.NewUpdatePasswordCommand(1, "yack1234")

	//When
	evt, err := user.ChangePassword(command)

	//Then
	is.Equal(fmt.Errorf("Invalid password"), err)
	is.Equal(nil, evt)
}

func TestUpdatePassword_returnErrorWhenPasswordTooShort(t *testing.T) {
	//Given
	is := is.New(t)
	ure := event.NewUserRegisteredEvent(1, 42, "jon", "doe", "jon.doe@gmail.com", "yack1234")
	user, _ := NewUserFromEvents([]event.UserEvent{ure})

	command := command.NewUpdatePasswordCommand(1, "yack")

	//When
	evt, err := user.ChangePassword(command)

	//Then
	is.Equal(fmt.Errorf("Too short password"), err)
	is.Equal(nil, evt)
}

func TestUpdateAge_returnAgeUpdated(t *testing.T) {
	//Given
	is := is.New(t)
	ure := event.NewUserRegisteredEvent(1, 42, "jon", "doe", "jon.doe@gmail.com", "yack1234")
	user, _ := NewUserFromEvents([]event.UserEvent{ure})

	command := command.NewUpdateAgeCommand(1, 43)

	//When
	evt, err := user.ChangeAge(*command)

	//Then
	is.NoErr(err)
	is.Equal(reflect.TypeOf(evt), reflect.TypeOf((*event.AgeUpdatedEvent)(nil)))
	is.Equal(1, evt.ID)
	is.Equal(43, evt.Age)
}

func TestUpdateAge_returnErrorWhenAgeLowerThanCurrent(t *testing.T) {
	//Given
	is := is.New(t)
	ure := event.NewUserRegisteredEvent(1, 42, "jon", "doe", "jon.doe@gmail.com", "yack1234")
	user, _ := NewUserFromEvents([]event.UserEvent{ure})

	command := command.NewUpdateAgeCommand(1, 41)

	//When
	evt, err := user.ChangeAge(*command)

	//Then
	is.Equal(err, fmt.Errorf("New age is lower than the current"))
	is.Equal(nil, evt)
}

func TestUpdateFirstname_returnFirstnameUpdated(t *testing.T) {
	//Given
	is := is.New(t)
	ure := event.NewUserRegisteredEvent(1, 42, "jon", "doe", "jon.doe@gmail.com", "yack1234")
	user, _ := NewUserFromEvents([]event.UserEvent{ure})

	cmd := command.NewUpdateFirstnameCommand(1, "jonnhy")

	//When
	evt, err := user.ChangeFirstname(*cmd)

	//Then
	is.NoErr(err)
	is.Equal(reflect.TypeOf(evt), reflect.TypeOf((*event.FirstnameUpdatedEvent)(nil)))
	is.Equal(evt.NewFirstname, "jonnhy")
}

func TestUpdateFirstname_returnErrorWhenSameFirstname(t *testing.T) {
	//Given
	is := is.New(t)
	ure := event.NewUserRegisteredEvent(1, 42, "jon", "doe", "jon.doe@gmail.com", "yack1234")
	user, _ := NewUserFromEvents([]event.UserEvent{ure})

	cmd := command.NewUpdateFirstnameCommand(1, "jon")

	//When
	evt, err := user.ChangeFirstname(*cmd)

	//Then
	is.Equal(err, fmt.Errorf("Firstname value incorrect"))
	is.Equal(evt, nil)
}

func TestUpdateLastname_returnLastnameUpdated(t *testing.T) {
	//Given
	is := is.New(t)
	ure := event.NewUserRegisteredEvent(1, 42, "jon", "doe", "jon.doe@gmail.com", "yack1234")
	user, _ := NewUserFromEvents([]event.UserEvent{ure})

	command := command.NewUpdateLastnameCommand(1, "doedoe")

	//When
	evt, err := user.ChangeLastname(*command)

	//Then
	is.NoErr(err)
	is.Equal(reflect.TypeOf(evt), reflect.TypeOf((*event.LastnameUpdatedEvent)(nil)))
	is.Equal(1, evt.ID)
	is.Equal("doedoe", evt.NewLastname)
}

func TestUpdateLastname_returnErrorWhenSameLastname(t *testing.T) {
	//Given
	is := is.New(t)
	ure := event.NewUserRegisteredEvent(1, 42, "jon", "doe", "jon.doe@gmail.com", "yack1234")
	user, _ := NewUserFromEvents([]event.UserEvent{ure})

	cmd := command.NewUpdateLastnameCommand(1, "doe")

	//When
	evt, err := user.ChangeLastname(*cmd)

	//Then
	is.Equal(err, fmt.Errorf("Lastname value incorrect"))
	is.Equal(evt, nil)
}
