package main

import (
	"fmt"
	"log"
	"sync"
	"time"

	"bitbucket.org/ValentinLethiot/userEvent/eventstore"
	"bitbucket.org/ValentinLethiot/userEvent/eventstore/redis_store"
)

var evt1 = eventstore.NewEvent(1, "ApplicationCreated", []byte{12})
var evt2 = eventstore.NewEvent(2, "ApplicationUpdated", []byte{99})
var evt3 = eventstore.NewEvent(3, "FeatureFlippingActivated", []byte{54})
var evt4 = eventstore.NewEvent(4, "MemberAdded", []byte{72})
var evt5 = eventstore.NewEvent(5, "ApiKeyCreated", []byte{000})

func main() {
	rp, err := redis_store.NewRedisPublisher("localhost:6379", "", 0)
	if err != nil {
		log.Fatalf(err.Error())
	}
	rs, err := redis_store.NewRedisSubscriber("localhost:6379", "", 0)
	if err != nil {
		log.Fatalf(err.Error())
	}

	ch, err := rs.Subscribe("mysm")
	if err != nil {
		log.Fatalf(err.Error())
	}
	ch2, err := rs.Subscribe("mystream")
	if err != nil {
		log.Fatalf(err.Error())
	}

	var wg sync.WaitGroup
	wg.Add(1)

	go func(channel <-chan *eventstore.Event) {
		for {
			fmt.Println("--Event Recieved from mysm--")
			fmt.Println("--", *<-channel, "--")
		}
	}(ch)
	go func(channel <-chan *eventstore.Event) {
		for {
			fmt.Println("--Event Recieved from mystream--")
			fmt.Println("--", *<-channel, "--")
		}
	}(ch2)

	go func(*sync.WaitGroup) {
		for {
			timer := time.NewTimer(5000 * time.Millisecond)
			<-timer.C
			rp.Publish("mysm", evt1)
			rp.Publish("mystream", evt5)
		}
		wg.Done()
	}(&wg)

	wg.Wait()
}
