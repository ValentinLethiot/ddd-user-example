package eventstore

type Publisher interface {
	Publish(topic string, events ...*Event) error
	Close() error
}

type Subscriber interface {
	Subscribe(topic string) (<-chan *Event, error)
	Close() error
}

type Event struct {
	ID        int
	EventType string
	Data      []byte
}

func NewEvent(id int, eventType string, data []byte) *Event {
	return &Event{
		ID:        id,
		EventType: eventType,
		Data:      data,
	}
}
