package redis_store

import (
	"log"

	"bitbucket.org/ValentinLethiot/userEvent/eventstore"
	"github.com/go-redis/redis"
)

type RedisPublisher struct {
	Client *redis.Client
}

func NewRedisPublisher(add, pass string, db int) (*RedisPublisher, error) {
	cli := redis.NewClient(&redis.Options{
		Addr:     add,
		Password: pass,
		DB:       db,
	})
	_, err := cli.Ping().Result()
	if err != nil {
		return nil, err
	}
	log.Println("Connected to Redis on port 6379")

	return &RedisPublisher{
		Client: cli,
	}, nil
}

func (rp RedisPublisher) Publish(topic string, events ...*eventstore.Event) error {
	for _, evt := range events {
		evtMap := map[string]interface{}{
			"ID":   evt.ID,
			"TYPE": evt.EventType,
			"DATA": evt.Data,
		}
		args := &redis.XAddArgs{
			Stream: topic,
			ID:     "*",
			Values: evtMap,
		}
		res := rp.Client.XAdd(args)
		if res.Err() != nil {
			return res.Err()
		}

		log.Println("Event sent")
	}
	return nil
}

func (rp RedisPublisher) Close() error {
	err := rp.Client.Close()
	if err != nil {
		return err
	}

	log.Println("Redis store closed successfully")
	return nil
}
