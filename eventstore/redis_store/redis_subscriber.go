package redis_store

import (
	"log"
	"strconv"

	"bitbucket.org/ValentinLethiot/userEvent/eventstore"
	"github.com/go-redis/redis"
)

type RedisSubscriber struct {
	Client         *redis.Client
	LastIDRecieved string
}

func NewRedisSubscriber(add, pass string, db int) (*RedisSubscriber, error) {
	cli := redis.NewClient(&redis.Options{
		Addr:     add,
		Password: pass,
		DB:       db,
	})
	_, err := cli.Ping().Result()
	if err != nil {
		return nil, err
	}
	log.Println("Subscriber connected to Redis on port 6379")

	return &RedisSubscriber{
		Client:         cli,
		LastIDRecieved: "0",
	}, nil
}

func (rs RedisSubscriber) Subscribe(topic string) (<-chan *eventstore.Event, error) {
	channel := make(chan *eventstore.Event)

	go func(channel chan *eventstore.Event) {
		for {
			args := redis.XReadArgs{
				Streams: []string{topic, rs.LastIDRecieved},
				Count:   1,
			}

			evts := rs.Client.XRead(&args)
			res := evts.Val()

			rs.LastIDRecieved = res[0].Messages[0].ID
			evtMap := res[0].Messages[0].Values

			id, _ := strconv.Atoi(evtMap["ID"].(string))
			evtType := evtMap["TYPE"].(string)
			data := []byte(evtMap["DATA"].(string))

			evt := eventstore.NewEvent(id, evtType, data)
			channel <- evt
		}
	}(channel)

	return channel, nil
}

func (rs RedisSubscriber) Close() error {
	err := rs.Client.Close()
	if err != nil {
		return err
	}
	log.Println("Redis store closed successfully")
	return nil
}
