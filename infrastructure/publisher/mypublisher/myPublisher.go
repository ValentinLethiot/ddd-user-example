package mypublisher

import (
	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/storeClient"
)

type MyPublisher struct {
	Store      storeClient.StorePub
}

func NewMyPublisher(store storeClient.StorePub) *MyPublisher {
	return &MyPublisher{
		Store:      store,
	}
}

func (mp *MyPublisher) StoreEvent(ev event.UserEvent) error {
	mp.Store.StoreEvent(ev)
	return nil
}

func (mp *MyPublisher) StoreEvents(ev []event.UserEvent) error {
	for _, evt := range ev {
		mp.Store.StoreEvent(evt)
	}
	return nil
}
