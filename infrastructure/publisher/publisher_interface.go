package publisher

import "bitbucket.org/ValentinLethiot/userEvent/domain/event"

type Publisher interface {
	StoreEvent(event.UserEvent) error
	StoreEvents([]event.UserEvent) error
}
