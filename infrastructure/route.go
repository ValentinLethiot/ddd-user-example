package infrastructure

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/ValentinLethiot/userEvent/domain"
	"bitbucket.org/ValentinLethiot/userEvent/domain/command"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/publisher"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/subscriber"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func GetRouter(pub publisher.Publisher, sub subscriber.Subscriber) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Route("/", func(r chi.Router) {
		r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
			writer.WriteHeader(http.StatusOK)
			writer.Write([]byte("Hello World"))
		})
	})

	r.Route("/user", func(r chi.Router) {

		r.Route("/all", func(r chi.Router) {
			r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
				projectionJSON, err := json.Marshal(sub.GetEvents())
				if err != nil {
					writer.WriteHeader(http.StatusInternalServerError)
					writer.Write([]byte(err.Error()))
					return
				}
				writer.WriteHeader(http.StatusOK)
				writer.Write([]byte(projectionJSON))
			})
		}) //fin /user/all

		r.Route("/add", func(r chi.Router) {
			r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
				fmt.Fprintf(writer,
					"<div style=\"margin:auto;width:300px;\">"+
						"<h1>Create User</h1>"+
						"<form method=\"POST\" style=\"padding:5px;\">"+
						"<p>LASTNAME : </p><input type=\"text\" name=\"ln\"></input><br>"+
						"<p>FIRSTNAME : </p><input type=\"text\" name=\"fn\"></input><br>"+
						"<p>AGE : </p><input type=\"text\" name=\"age\"></input><br>"+
						"<p>MAIL : </p><input type=\"text\" name=\"mail\"></input><br>"+
						"<p>PASSWORD : </p><input type=\"text\" name=\"pass\"></input><br>"+
						"</br><input type=\"submit\" value=\"ADD\">"+
						"</form></div>")
			})

			r.Post("/", func(writer http.ResponseWriter, request *http.Request) {
				ln := request.PostFormValue("ln")
				fn := request.PostFormValue("fn")
				agestr := request.PostFormValue("age")
				mail := request.PostFormValue("mail")
				pass := request.PostFormValue("pass")

				age, err := strconv.Atoi(agestr)
				if err != nil {
					writer.WriteHeader(http.StatusInternalServerError)
					writer.Write([]byte(err.Error()))
					return
				}

				aggregate := domain.NewUser()

				cmd := command.NewRegisterUserCommand(0, age, fn, ln, mail, pass)
				evt, err := aggregate.Register(cmd)
				if err != nil {
					writer.WriteHeader(http.StatusInternalServerError)
					writer.Write([]byte(err.Error()))
					return
				}
				err = pub.StoreEvent(evt)
				if err != nil {
					writer.WriteHeader(http.StatusInternalServerError)
					writer.Write([]byte(err.Error()))
					return
				}

				//writer.WriteHeader(http.StatusOK)
				writer.Write([]byte("User added"))
			})
		}) //fin /user/add

		r.Route("/{id}", func(r chi.Router) {
			r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
				idstr := chi.URLParam(request, "id")
				id, err := strconv.Atoi(idstr)
				if err != nil {
					writer.WriteHeader(http.StatusInternalServerError)
					writer.Write([]byte(err.Error()))
					return
				}

				projectionJSON, err := json.Marshal(sub.GetEventsByUserID(id))
				if err != nil {
					writer.WriteHeader(http.StatusInternalServerError)
					writer.Write([]byte(err.Error()))
					return
				}
				writer.WriteHeader(http.StatusOK)
				writer.Write([]byte(projectionJSON))
			})

			r.Route("/newfn", func(r chi.Router) {
				r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
					fmt.Fprintf(writer,
						"<div style=\"margin:auto;width:300px;\">"+
							"<h1>Change Firstname</h1>"+
							"<form method=\"POST\">"+
							"<p>NEW FIRSTNAME : </p><input type=\"text\" name=\"fn\"></input><br>"+
							"</br><input type=\"submit\" value=\"CHANGE\">"+
							"</form></div>")
				})

				r.Post("/", func(writer http.ResponseWriter, request *http.Request) {
					nfn := request.PostFormValue("fn")
					idstr := chi.URLParam(request, "id")
					id, err := strconv.Atoi(idstr)
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}

					aggregate, err := domain.NewUserFromEvents(sub.GetEventsByUserID(id))
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}

					cmd := command.NewUpdateFirstnameCommand(id, nfn)

					evt, err := aggregate.ChangeFirstname(*cmd)
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}
					pub.StoreEvent(evt)

					writer.WriteHeader(http.StatusOK)
					writer.Write([]byte("Firstname changed"))
				})
			}) //fin /user/id/newfn

			r.Route("/newln", func(r chi.Router) {
				r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
					fmt.Fprintf(writer,
						"<div style=\"margin:auto;width:300px;\">"+
							"<h1>Change Lastname</h1>"+
							"<form method=\"POST\">"+
							"<p>NEW LASTNAME : </p><input type=\"text\" name=\"ln\"></input><br>"+
							"</br><input type=\"submit\" value=\"CHANGE\">"+
							"</form></div>")
				})

				r.Post("/", func(writer http.ResponseWriter, request *http.Request) {
					nln := request.PostFormValue("ln")
					idstr := chi.URLParam(request, "id")
					id, err := strconv.Atoi(idstr)
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}

					aggregate, err := domain.NewUserFromEvents(sub.GetEventsByUserID(id))
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}

					cmd := command.NewUpdateLastnameCommand(id, nln)

					evt, err := aggregate.ChangeLastname(*cmd)
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}
					pub.StoreEvent(evt)

					writer.WriteHeader(http.StatusOK)
					writer.Write([]byte("Lastname changed"))
				})
			}) //fin /user/id/newln

			r.Route("/newpass", func(r chi.Router) {
				r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
					fmt.Fprintf(writer,
						"<div style=\"margin:auto;width:300px;\">"+
							"<h1>Change Password</h1>"+
							"<form method=\"POST\">"+
							"<p>NEW PASSWORD : </p><input type=\"text\" name=\"pass\"></input><br>"+
							"</br><input type=\"submit\" value=\"CHANGE\">"+
							"</form></div>")
				})

				r.Post("/", func(writer http.ResponseWriter, request *http.Request) {
					npass := request.PostFormValue("pass")
					idstr := chi.URLParam(request, "id")
					id, err := strconv.Atoi(idstr)
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}

					aggregate, err := domain.NewUserFromEvents(sub.GetEventsByUserID(id))
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}

					cmd := command.NewUpdatePasswordCommand(id, npass)

					evt, err := aggregate.ChangePassword(cmd)
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}
					pub.StoreEvent(evt)

					writer.WriteHeader(http.StatusOK)
					writer.Write([]byte("Password changed"))
				})
			}) //fin /user/id/newpass

			r.Route("/newage", func(r chi.Router) {
				r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
					fmt.Fprintf(writer,
						"<div style=\"margin:auto;width:300px;\">"+
							"<h1>Change Age</h1>"+
							"<form method=\"POST\">"+
							"<p>NEW AGE : </p><input type=\"text\" name=\"age\"></input><br>"+
							"</br><input type=\"submit\" value=\"CHANGE\">"+
							"</form></div>")
				})

				r.Post("/", func(writer http.ResponseWriter, request *http.Request) {
					agestr := request.PostFormValue("age")
					age, err := strconv.Atoi(agestr)
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}
					idstr := chi.URLParam(request, "id")
					id, err := strconv.Atoi(idstr)
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}

					aggregate, err := domain.NewUserFromEvents(sub.GetEventsByUserID(id))
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}

					cmd := command.NewUpdateAgeCommand(id, age)

					evt, err := aggregate.ChangeAge(*cmd)
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}
					pub.StoreEvent(evt)

					writer.WriteHeader(http.StatusOK)
					writer.Write([]byte("Age changed"))
				})
			}) //fin /user/id/newage

			r.Route("/us", func(r chi.Router) {
				r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
					idstr := chi.URLParam(request, "id")
					id, err := strconv.Atoi(idstr)
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}

					aggregate, err := domain.NewUserFromEvents(sub.GetEventsByUserID(id))
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}
					userJSON, err := json.Marshal(aggregate)
					if err != nil {
						writer.WriteHeader(http.StatusInternalServerError)
						writer.Write([]byte(err.Error()))
						return
					}
					writer.WriteHeader(http.StatusOK)
					writer.Write([]byte(userJSON))
				})
			}) //fin /user/id/us
		}) //fin /user/id
	}) //fin /user

	return r
}
