package infrastructure

import (
	"net/http/httptest"
	"testing"

	"bitbucket.org/ValentinLethiot/userEvent/domain/projection"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/publisher/mypublisher"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/store/memorystore"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/storeClient"
	"github.com/matryer/is"
)

func TestHelloWorld_returnsOK(t *testing.T) {
	//Given
	is := is.New(t)
	projection := projection.NewUserDetailsProjection()
	store := memorystore.NewEventStoreMemory()
	storePub := storeClient.NewStorePub(store, "mystream")
	publisher := mypublisher.NewMyPublisher(*storePub)
	r := GetRouter(publisher, projection)

	//When
	request := httptest.NewRequest("GET", "/", nil)
	response := httptest.NewRecorder()
	r.ServeHTTP(response, request)

	//Then
	is.Equal(response.Code, 200)
	is.Equal(response.Body.String(), "Hello World")
}
