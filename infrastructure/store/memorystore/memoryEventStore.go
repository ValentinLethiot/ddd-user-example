package memorystore

import (
	"fmt"

	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/subscriber"
)

type EventStoreMemory struct {
	Store       []event.UserEvent
	Subscribers []subscriber.Subscriber
}

func NewEventStoreMemory() *EventStoreMemory {
	st := make([]event.UserEvent, 0)
	sub := make([]subscriber.Subscriber, 0)

	return &EventStoreMemory{
		Store:       st,
		Subscribers: sub,
	}
}

func (es *EventStoreMemory) AddEvent(evt event.UserEvent) error {
	es.Store = append(es.Store, evt)

	for i := 0; i < len(es.Subscribers); i++ {
		es.Subscribers[i].ApplyEvent(evt)
	}

	return nil
}

func (es *EventStoreMemory) AddEvents(evts []event.UserEvent) error {
	es.Store = append(es.Store, evts...)

	for i := 0; i < len(es.Subscribers); i++ {
		es.Subscribers[i].ApplyEvents(evts)
	}

	return nil
}

func (es *EventStoreMemory) GetEvents() []event.UserEvent {
	return es.Store
}

func (es *EventStoreMemory) GetEvent(id int) (event.UserEvent, error) {
	if len(es.Store) <= id {
		return nil, fmt.Errorf("This event doesn't exist")
	}
	return es.Store[id-1], nil
}

func (es *EventStoreMemory) AddSubscriber(sub subscriber.Subscriber) {
	es.Subscribers = append(es.Subscribers, sub)
}

func (es *EventStoreMemory) GetEventsByUserID(id int) []event.UserEvent {
	tab := make([]event.UserEvent, 0)
	for i := 0; i < len(es.Store); i++ {
		if es.Store[i].GetID() == id {
			tab = append(tab, es.Store[i])
		}
	}
	return tab
}
