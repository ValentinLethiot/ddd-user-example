package memorystore

import (
	"fmt"
	"reflect"
	"testing"

	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
	"bitbucket.org/ValentinLethiot/userEvent/domain/projection"
	"github.com/matryer/is"
)

func TestAddEvent_returnsEventAdded(t *testing.T) {
	//Given
	is := is.New(t)
	store := NewEventStoreMemory()
	evt := event.UserRegisteredEvent{
		ID:        1,
		Firstname: "jon",
		Lastname:  "doe",
		Age:       100,
		Email:     "jon.doe@gmail.com",
		Password:  "abcdefhij",
	}

	//When
	err := store.AddEvent(evt)

	//Then
	is.NoErr(err)
	is.Equal(1, len(store.Store))
}

func TestAddEvents_returnsEventsAdded(t *testing.T) {
	//Given
	is := is.New(t)
	store := NewEventStoreMemory()
	evt := []event.UserEvent{
		event.UserRegisteredEvent{
			ID:        1,
			Firstname: "jon",
			Lastname:  "doe",
			Age:       100,
			Email:     "jon.doe@gmail.com",
			Password:  "abcdefhij",
		},
		event.UserRegisteredEvent{
			ID:        2,
			Firstname: "jojo",
			Lastname:  "dodo",
			Age:       10,
			Email:     "jojo.dodo@gmail.com",
			Password:  "password",
		},
		event.AgeUpdatedEvent{
			ID:  2,
			Age: 11,
		},
	}

	//When
	err := store.AddEvents(evt)

	//Then
	is.NoErr(err)
	is.Equal(3, len(store.Store))
	is.Equal(reflect.TypeOf(store.Store[0]), reflect.TypeOf((event.UserRegisteredEvent{})))
	is.Equal(reflect.TypeOf(store.Store[1]), reflect.TypeOf((event.UserRegisteredEvent{})))
	is.Equal(reflect.TypeOf(store.Store[2]), reflect.TypeOf((event.AgeUpdatedEvent{})))
}

func TestGetEvents_returnsEvents(t *testing.T) {
	//Given
	is := is.New(t)
	store := NewEventStoreMemory()
	evt := []event.UserEvent{
		event.UserRegisteredEvent{
			ID:        1,
			Firstname: "jon",
			Lastname:  "doe",
			Age:       100,
			Email:     "jon.doe@gmail.com",
			Password:  "abcdefhij",
		},
		event.UserRegisteredEvent{
			ID:        2,
			Firstname: "jojo",
			Lastname:  "dodo",
			Age:       10,
			Email:     "jojo.dodo@gmail.com",
			Password:  "password",
		},
		event.AgeUpdatedEvent{
			ID:  2,
			Age: 11,
		},
	}
	store.AddEvents(evt)

	//When
	evts := store.GetEvents()

	//Then
	is.Equal(3, len(evts))
}

func TestGetEvent_returnsEvent(t *testing.T) {
	//Given
	is := is.New(t)
	store := NewEventStoreMemory()
	evt := []event.UserEvent{
		event.UserRegisteredEvent{
			ID:        1,
			Firstname: "jon",
			Lastname:  "doe",
			Age:       100,
			Email:     "jon.doe@gmail.com",
			Password:  "abcdefhij",
		},
		event.UserRegisteredEvent{
			ID:        2,
			Firstname: "jojo",
			Lastname:  "dodo",
			Age:       10,
			Email:     "jojo.dodo@gmail.com",
			Password:  "password",
		},
		event.AgeUpdatedEvent{
			ID:  2,
			Age: 11,
		},
	}
	store.AddEvents(evt)

	//When
	myEvt, err := store.GetEvent(2)

	//Then
	is.NoErr(err)
	is.Equal(myEvt.GetID(), 2)

	ure := myEvt.(event.UserRegisteredEvent)
	is.Equal(reflect.TypeOf(ure), reflect.TypeOf((event.UserRegisteredEvent{})))
	is.Equal(ure.Firstname, "jojo")
	is.Equal(ure.Lastname, "dodo")
	is.Equal(ure.Age, 10)
	is.Equal(ure.Email, "jojo.dodo@gmail.com")
	is.Equal(ure.Password, "password")
}

func TestGetEvent_returnsNotFound(t *testing.T) {
	//Given
	is := is.New(t)
	store := NewEventStoreMemory()
	evt := []event.UserEvent{
		event.UserRegisteredEvent{
			ID:        1,
			Firstname: "jon",
			Lastname:  "doe",
			Age:       100,
			Email:     "jon.doe@gmail.com",
			Password:  "abcdefhij",
		},
		event.UserRegisteredEvent{
			ID:        2,
			Firstname: "jojo",
			Lastname:  "dodo",
			Age:       10,
			Email:     "jojo.dodo@gmail.com",
			Password:  "password",
		},
		event.AgeUpdatedEvent{
			ID:  2,
			Age: 11,
		},
	}
	store.AddEvents(evt)

	//When
	myEvt, err := store.GetEvent(4)

	//Then
	is.Equal(myEvt, nil)
	is.Equal(fmt.Errorf("This event doesn't exist"), err)
}

func TestAddSubscriber_returnsSubscriberAdded(t *testing.T) {
	//Given
	is := is.New(t)
	store := NewEventStoreMemory()
	sub := projection.NewUserDetailsProjection()

	//When
	store.AddSubscriber(sub)

	//Then
	is.Equal(len(store.Subscribers), 1)
	is.Equal(reflect.TypeOf(store.Subscribers[0]), reflect.TypeOf((*projection.UsersDetailsProjection)(nil)))
}

func TestGetEventsByUserID_returnsOK(t *testing.T) {
	//Given
	is := is.New(t)
	store := NewEventStoreMemory()
	evt := []event.UserEvent{
		event.UserRegisteredEvent{
			ID:        1,
			Firstname: "jon",
			Lastname:  "doe",
			Age:       100,
			Email:     "jon.doe@gmail.com",
			Password:  "abcdefhij",
		},
		event.UserRegisteredEvent{
			ID:        2,
			Firstname: "jojo",
			Lastname:  "dodo",
			Age:       10,
			Email:     "jojo.dodo@gmail.com",
			Password:  "password",
		},
		event.AgeUpdatedEvent{
			ID:  2,
			Age: 11,
		},
	}
	store.AddEvents(evt)

	//When
	evts := store.GetEventsByUserID(2)

	//Then
	is.Equal(len(evts), 2)
	is.Equal(reflect.TypeOf(evts[0]), reflect.TypeOf((event.UserRegisteredEvent{})))
	is.Equal(reflect.TypeOf(evts[1]), reflect.TypeOf((event.AgeUpdatedEvent{})))
}
