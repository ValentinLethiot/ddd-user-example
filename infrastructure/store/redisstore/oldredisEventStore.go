package redisstore

/*
import (
	"encoding/json"
	"fmt"
	"log"

	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
	"github.com/fatih/structs"
	"github.com/go-redis/redis"
)

type Event struct {
	Type string
	Data []byte
}

func NewEvent(typ string, data []byte) Event {
	return Event{
		Type: typ,
		Data: data,
	}
}

type RedisStore struct {
	Client     *redis.Client
	StreamName string
}

func NewRedisStore(address, pass, grpName, stmName string, db int) (*RedisStore, error) {
	cli := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: pass,
		DB:       db,
	})
	_, err := cli.Ping().Result()
	if err != nil {
		return nil, err
	}
	log.Println("Connected to Redis on port 6379")

	//create the consumers group
	st := cli.XGroupCreate(stmName, grpName, "0")
	if st.Err() != nil {
		fmt.Println(st.Err().Error())
	}

	return &RedisStore{
		Client:     cli,
		StreamName: stmName,
	}, nil
}

func (rs *RedisStore) AddEvent(evt event.UserEvent) error {
	var eventType string
	switch evt.(type) {
	case *event.UserRegisteredEvent:
		eventType = "UserRegisteredEvent"
	case *event.PasswordUpdatedEvent:
		eventType = "PasswordUpdatedEvent"
	case *event.FirstnameUpdatedEvent:
		eventType = "FirstnameUpdatedEvent"
	case *event.LastnameUpdatedEvent:
		eventType = "LastnameUpdatedEvent"
	case *event.AgeUpdatedEvent:
		eventType = "AgeUpdatedEvent"
	default:
		eventType = ""
		return fmt.Errorf("Unknow event")
	}
	evtJson, err := json.Marshal(evt)
	if err != nil {
		return fmt.Errorf(err.Error())
	}

	e := NewEvent(eventType, evtJson)
	args := &redis.XAddArgs{
		Stream: rs.StreamName,
		ID:     "*",
		Values: structs.Map(e),
	}

	res := rs.Client.XAdd(args)
	if res.Err() != nil {
		return res.Err()
	}

	return nil
}

/*
func (rs *RedisStore) AddEvents(evts []event.UserEvent) error {
	for _, evt := range evts {
		err := rs.AddEvent(evt)
		if err != nil {
			return err
		}
	}
	return nil
}

func (rs *RedisStore) GetEvents() []event.UserEvent {
	lesEvents := make([]event.UserEvent, 0)
	args := redis.XReadGroupArgs{
		Group:   rs.GroupName,
		Streams: []string{rs.StreamName, ">"},
	}
	evts := rs.Client.XReadGroup(&args)
	res := evts.Val()
	for i := 0; i < len(res); i++ {
		lesEvents = append(lesEvents, mapToUserEvent(res[i].Messages[0].Values))
	}
	return lesEvents
}

func (rs *RedisStore) GetEvent(i int) (event.UserEvent, error) {
	evts := rs.GetEvents()
	if len(evts) > i {
		return nil, fmt.Errorf("No enough event")
	}
	return evts[i], nil
}

func (rs *RedisStore) GetEventsByUserID(id int) []event.UserEvent {
	evtsReturn := make([]event.UserEvent, 0)
	evts := rs.GetEvents()

	for i := 0; i < len(evts); i++ {
		if evts[i].GetID() == id {
			evtsReturn = append(evtsReturn, evts[i])
		}
	}
	return evtsReturn
}

func mapToUserEvent(mapEvent map[string]interface{}) event.UserEvent {
	switch mapEvent["Type"] {
	case "UserRegisteredEvent":
		var f map[string]interface{}
		json.Unmarshal([]byte(mapEvent["Data"].(string)), &f)
		id := int(f["ID"].(float64))
		age := int(f["Age"].(float64))
		fn := fmt.Sprint(f["Firstname"])
		ln := fmt.Sprint(f["Lastname"])
		mail := fmt.Sprint(f["Email"])
		pass := fmt.Sprint(f["Password"])
		return event.NewUserRegisteredEvent(id, age, fn, ln, mail, pass)

	case "PasswordUpdatedEvent":
		var f map[string]interface{}
		json.Unmarshal([]byte(mapEvent["Data"].(string)), &f)
		flo := int(f["ID"].(float64))
		np := fmt.Sprint(f["NewPassword"])
		return event.NewPasswordUpdatedEvent(flo, np)

	case "FirstnameUpdatedEvent":
		var f map[string]interface{}
		json.Unmarshal([]byte(mapEvent["Data"].(string)), &f)
		id := int(f["ID"].(float64))
		nfn := fmt.Sprint(f["NewFirstname"])
		return event.NewFirstnameUpdatedEvent(id, nfn)

	case "LastnameUpdatedEvent":
		var f map[string]interface{}
		json.Unmarshal([]byte(mapEvent["Data"].(string)), &f)
		id := int(f["ID"].(float64))
		nln := fmt.Sprint(f["NewLastname"])
		return event.NewLastnameUpdatedEvent(id, nln)

	case "AgeUpdatedEvent":
		var f map[string]interface{}
		json.Unmarshal([]byte(mapEvent["Data"].(string)), &f)
		id := int(f["ID"].(float64))
		age := int(f["Age"].(float64))
		return event.NewAgeUpdatedEvent(id, age)
	default:
		return nil
	}
}
*/
