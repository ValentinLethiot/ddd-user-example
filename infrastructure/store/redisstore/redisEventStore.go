package redisstore

type RedisStore struct {
	Address string
	Pass    string
	DB      int
}

func NewRedisStore(add, pass string, db int) *RedisStore {
	return &RedisStore{
		Address: add,
		Pass:    pass,
		DB:      db,
	}
}
