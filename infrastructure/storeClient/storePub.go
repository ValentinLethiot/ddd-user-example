package storeClient

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/store/redisstore"
	"bitbucket.org/ValentinLethiot/userEvent/proto"
	"github.com/fatih/structs"
	"github.com/go-redis/redis"
)

/*
type Event struct {
	Type string
	Data []byte
}

func NewEvent(typ string, data []byte) Event {
	return Event{
		Type: typ,
		Data: data,
	}
}*/

type StorePub struct {
	Client     *redis.Client
	Store      redisstore.RedisStore
	StreamName string
}

func NewStorePub(store redisstore.RedisStore, stream string) *StorePub {
	cli := redis.NewClient(&redis.Options{
		Addr:     store.Address,
		Password: store.Pass,
		DB:       store.DB,
	})
	return &StorePub{
		Client:     cli,
		Store:      store,
		StreamName: stream,
	}
}

func (sp *StorePub) StoreEvent(evt event.UserEvent) error {
	var eventType string
	switch evt.(type) {
	case *event.UserRegisteredEvent:
		eventType = "UserRegisteredEvent"
	case *event.PasswordUpdatedEvent:
		eventType = "PasswordUpdatedEvent"
	case *event.FirstnameUpdatedEvent:
		eventType = "FirstnameUpdatedEvent"
	case *event.LastnameUpdatedEvent:
		eventType = "LastnameUpdatedEvent"
	case *event.AgeUpdatedEvent:
		eventType = "AgeUpdatedEvent"
	default:
		eventType = ""
		return fmt.Errorf("Unknow event")
	}
	evtJson, err := json.Marshal(evt)
	if err != nil {
		return fmt.Errorf(err.Error())
	}

	e := proto.Event{
		Id:        int64(evt.GetID()),
		EventType: eventType,
		Data:      evtJson,
	}

	args := &redis.XAddArgs{
		Stream: sp.StreamName,
		ID:     "*",
		Values: structs.Map(e),
	}

	res := sp.Client.XAdd(args)
	if res.Err() != nil {
		fmt.Println("HERE")
		fmt.Println(res.Err())
		return res.Err()
	}

	fmt.Println(res)
	return nil
}
