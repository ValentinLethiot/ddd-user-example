package storeClient

import (
	"encoding/json"
	"fmt"
	"sync"

	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/store/redisstore"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/subscriber"
	"github.com/go-redis/redis"
)

type StoreSub struct {
	Store      redisstore.RedisStore
	StreamName string
	GroupName  string
	Subscriber subscriber.Subscriber
}

func NewStoreSub(store redisstore.RedisStore, stream, group string, sub subscriber.Subscriber) *StoreSub {
	return &StoreSub{
		Store:      store,
		StreamName: stream,
		GroupName:  group,
		Subscriber: sub,
	}
}

func (sc *StoreSub) ReadMyStore(wg *sync.WaitGroup) {
	cli := redis.NewClient(&redis.Options{
		Addr:     sc.Store.Address,
		Password: sc.Store.Pass,
		DB:       sc.Store.DB,
	})

	//create the consumers group
	st := cli.XGroupCreate(sc.StreamName, sc.GroupName, "0")
	if st.Err() != nil {
		fmt.Println("ici")
		fmt.Println(st.Err().Error())
	}

	args := redis.XReadGroupArgs{
		Group:   sc.GroupName,
		Streams: []string{sc.StreamName, ">"},
	}
	i := 0
	for {
		res := cli.XReadGroup(&args)
		if res.Err() != nil {
			fmt.Println(res.Err())
		}

		val := res.Val()
		fmt.Println(val[0].Messages[0].Values)
		sc.Subscriber.ApplyEvent(mapToUserEvent(val[0].Messages[0].Values))
		i++
	}
	wg.Done()
}

func mapToUserEvent(mapEvent map[string]interface{}) event.UserEvent {
	switch mapEvent["EventType"] {
	case "UserRegisteredEvent":
		var f map[string]interface{}
		json.Unmarshal([]byte(mapEvent["Data"].(string)), &f)
		id := int(f["ID"].(float64))
		age := int(f["Age"].(float64))
		fn := fmt.Sprint(f["Firstname"])
		ln := fmt.Sprint(f["Lastname"])
		mail := fmt.Sprint(f["Email"])
		pass := fmt.Sprint(f["Password"])
		return event.NewUserRegisteredEvent(id, age, fn, ln, mail, pass)

	case "PasswordUpdatedEvent":
		var f map[string]interface{}
		json.Unmarshal([]byte(mapEvent["Data"].(string)), &f)
		flo := int(f["ID"].(float64))
		np := fmt.Sprint(f["NewPassword"])
		return event.NewPasswordUpdatedEvent(flo, np)

	case "FirstnameUpdatedEvent":
		var f map[string]interface{}
		json.Unmarshal([]byte(mapEvent["Data"].(string)), &f)
		id := int(f["ID"].(float64))
		nfn := fmt.Sprint(f["NewFirstname"])
		return event.NewFirstnameUpdatedEvent(id, nfn)

	case "LastnameUpdatedEvent":
		var f map[string]interface{}
		json.Unmarshal([]byte(mapEvent["Data"].(string)), &f)
		id := int(f["ID"].(float64))
		nln := fmt.Sprint(f["NewLastname"])
		return event.NewLastnameUpdatedEvent(id, nln)

	case "AgeUpdatedEvent":
		var f map[string]interface{}
		json.Unmarshal([]byte(mapEvent["Data"].(string)), &f)
		id := int(f["ID"].(float64))
		age := int(f["Age"].(float64))
		return event.NewAgeUpdatedEvent(id, age)
	default:
		return nil
	}
}
