package mySubscriber

import (
	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
	"bitbucket.org/ValentinLethiot/userEvent/domain/projection"
)

type MySubscriber struct {
	Projection     *projection.UsersDetailsProjection
	SubscriberName string
}

func NewMySubscriber(proj *projection.UsersDetailsProjection, name string) *MySubscriber {
	return &MySubscriber{
		Projection:     proj,
		SubscriberName: name,
	}
}

func (ms *MySubscriber) ApplyEvent(evt event.UserEvent) {
	ms.Projection.ApplyEvent(evt)
}

func (ms *MySubscriber) ApplyEvents(evts []event.UserEvent) {
	ms.Projection.ApplyEvents(evts)
}
