package subscriber

import (
	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
)

type Subscriber interface {
	ApplyEvent(event.UserEvent)
	ApplyEvents([]event.UserEvent)
	GetEvents() []event.UserEvent
	GetEventsByUserID(int) []event.UserEvent
}
