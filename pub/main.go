package main

import (
	"fmt"
	"sync"
	"time"

	"bitbucket.org/ValentinLethiot/userEvent/domain/event"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/publisher/mypublisher"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/store/redisstore"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/storeClient"
)

func main() {
	redstore := redisstore.NewRedisStore("localhost:6379", "", 0)
	storePub := storeClient.NewStorePub(*redstore, "mystream")
	publisher := mypublisher.NewMyPublisher(*storePub)
	i := 0

	var wg sync.WaitGroup
	wg.Add(1)

	go func(*sync.WaitGroup) {
		for {
			timer := time.NewTimer(1000 * time.Millisecond)
			<-timer.C
			evt := event.NewUserRegisteredEvent(i, i*10, "myFirstname", "myLastname", "a@b.c", "azertyuiop")
			fmt.Println("evt Sent: ", i)
			err := publisher.StoreEvent(evt)
			if err != nil {
				fmt.Println(err.Error())
			}
			i++
		}
		wg.Done()
	}(&wg)
	wg.Wait()
}
