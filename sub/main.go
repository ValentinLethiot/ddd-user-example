package main

import (
	"sync"

	"bitbucket.org/ValentinLethiot/userEvent/domain/projection"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/store/redisstore"
	"bitbucket.org/ValentinLethiot/userEvent/infrastructure/storeClient"
)

func main() {
	store := redisstore.NewRedisStore("localhost:6379", "", 0)
	projection := projection.NewUserDetailsProjection()

	cli := storeClient.NewStoreSub(*store, "mystream", "myGroup", projection)
	var wg sync.WaitGroup
	wg.Add(1)

	go cli.ReadMyStore(&wg)

	wg.Wait()
}
