package main

import (
	"fmt"
	"sync"
)

func writeAndSend(chan2 chan<- int, wg *sync.WaitGroup) {
	for i := 0; i < 3; i++ {
		chan2 <- i
	}
	wg.Done()
}

func readAndPrint(chan2 <-chan int, wg *sync.WaitGroup) {
	for i := 0; i < 3; i++ {
		val := <-chan2
		fmt.Println("Valeur de val : ", val)
	}
	wg.Done()
}

func main() {
	var wg sync.WaitGroup

	//chan1 := make(chan int)
	chan2 := make(chan int)

	go writeAndSend(chan2, &wg)
	wg.Add(1)
	go readAndPrint(chan2, &wg)
	wg.Add(1)

	wg.Wait()
	//close(chan1)
	close(chan2)
}
